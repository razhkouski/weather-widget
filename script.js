const temperatureDescription = document.querySelector('.temperature-description'),
      temperatureDegree = document.querySelector('.temperature-degree'),
      locationTimezone = document.querySelector('.location-timezone'),
      temperature = document.querySelector('.temperature'),
      loc = document.querySelector('.location'),
      weatherIcon = document.querySelector('.icon'),
      currentTime = document.querySelector('.time'),      
      threeDaysWeatherForecastBlock = document.querySelector('.three-days-weather-forecast'),
      hideWeatherForecastButton = document.querySelector('.hide-weather-forecast-btn'),
      minskWeatherForecastButton = document.querySelector('.current-weather-Minsk-btn'),
      localWeatherForecastButton = document.querySelector('.current-weather-location-btn'),
      threeDaysWeatherForecastButton = document.querySelector('.three-days-weather-forecast-btn');

class WeatherWidget {
    constructor(city) {
        this.city = city;
    }

    getWeatherForecast() {
        fetch(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&lang=ru&units=metric&appid=42d796da0d435d66c1ce6b7bd9894a7e`)
            .then(response => response.json())
            .then(data => fillWeatherForecastBlock(data))
            .catch(e => console.log(`Error: ${e.message}.`))
    }
}

class LocalWeatherForecast extends WeatherWidget {
    constructor(city) {
        super(city);
    }

    getWeatherForecast(lat, lon) {
        fetch(`http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&lang=ru&units=metric&appid=42d796da0d435d66c1ce6b7bd9894a7e`)
            .then(resp => resp.json())
            .then(resp => {
                fillWeatherForecastBlock(resp);
                temperature.style.display = 'block';
            })
            .catch(e => console.log(`Error: ${e.message}.`))
    }
}

class ThreeDaysWeatherForecast extends WeatherWidget {
    constructor(city) {
        super(city)
    }

    getWeatherForecast() {
        fetch(`http://api.openweathermap.org/data/2.5/forecast?q=${this.city}&lang=ru&units=metric&appid=42d796da0d435d66c1ce6b7bd9894a7e`)
            .then(resp => resp.json())
            .then(resp => filterThreeDaysWeatherForecast(resp))
            .catch(e => console.log(`Error: ${e.message}.`))
    }
}

function fillWeatherForecastBlock(data) {
    const { main, name, weather, wind } = data;
    temperatureDegree.textContent = `${Math.round(parseFloat(main.temp))}°C`;
    locationTimezone.textContent = `${name}`;
    temperatureDescription.textContent = `${weather[0].description.toUpperCase()}`;
    weatherIcon.src = `http://openweathermap.org/img/wn/${weather[0].icon}@2x.png`;
    // windSpeed.textContent = `Ветер: ${wind.speed} м/с`;
}

function filterThreeDaysWeatherForecast(info) {
    const { list } = info;
    list.slice(0, 24).map(item => {
        if (item.dt_txt.includes('12:00:00')) {
            createBlockForOneDay(item)
        }
    })
}

function createBlockForOneDay(item) {
    const { dt_txt, main, weather, wind } = item;
    console.log(item);
    const elem = `<div class="day">
                    <h1 >${dt_txt.slice(5, 7)}.${dt_txt.slice(8, 10)}</h1>
                    <div class="degree">${Math.round(parseFloat(main.temp))}°C</div>
                    <div><img src="http://openweathermap.org/img/wn/${weather[0].icon}@2x.png" alt="current weather icon"></div>
                    <div class="description">${weather[0].description}</div>
                </div>`;
    if (document.querySelectorAll('.day').length < 3) threeDaysWeatherForecastBlock.insertAdjacentHTML('beforeend', elem);
}

hideWeatherForecastButton.onclick = () => {
    temperature.style.display = 'none';
    loc.style.display = 'none';
    threeDaysWeatherForecastBlock.style.display = 'none';
};

const weatherWidgetMinsk = new WeatherWidget('minsk');
weatherWidgetMinsk.getWeatherForecast();
minskWeatherForecastButton.onclick = () => {
    weatherWidgetMinsk.getWeatherForecast();
    temperature.style.display = 'block';
};

const localWeatherForecast = new LocalWeatherForecast('minsk');
localWeatherForecastButton.onclick = () => {
    function success(position) {
        const lat = position.coords.latitude;
        const lon = position.coords.longitude;
        localWeatherForecast.getWeatherForecast(lat, lon);
    }
    navigator.geolocation.getCurrentPosition(success);
};

const threeDaysWeatherForecast = new ThreeDaysWeatherForecast('minsk');
threeDaysWeatherForecastButton.onclick = () => threeDaysWeatherForecast.getWeatherForecast();